#include "loadCell.h"
#include <WiFi.h>
#include <WebSocketsServer.h>

#define WS 1

// Constants
const char* ssid = "Whisker-Corporate"; //Enter SSID
const char* password = "auburn_M30W!"; //Enter Password

HX711 scale;

const uint8_t dataPin = 27;//3;
const uint8_t clockPin = 26;//4;
volatile long weight = 0;

uint8_t newNum = NULL;

// Globals
WebSocketsServer webSocket = WebSocketsServer(80);


// Called when receiving any WebSocket message
void onWebSocketEvent(uint8_t num,
                      WStype_t type,
                      uint8_t * payload,
                      size_t length) {

  // Figure out the type of WebSocket event
  switch(type) {

    // Client has disconnected
    case WStype_DISCONNECTED:
      Serial.printf("[%u] Disconnected!\n", num);
      break;

    // New client has connected
    case WStype_CONNECTED:
      {
        IPAddress ip = webSocket.remoteIP(num);
        webSocket.sendTXT(num, "Connecting...");
        Serial.printf("[%u] Connection from ", num);
        Serial.println(ip.toString());
        webSocket.sendTXT(num, "Connected");
        //find connected clients id only speak the the ast connected client.
        newNum = num;
      }
      break;

    // Echo text message back to client
    case WStype_TEXT:
    {
      Serial.printf("[%d] DATA: %s\n",num, payload);
      webSocket.sendTXT(num, payload);
      uint8_t rawData = atoi((char *) payload);
      if (rawData == 100){
        webSocket.sendTXT(newNum, "Calibrating");
        scale.callibrate_scale(2000);
      }
      else if (rawData == 200){
        webSocket.sendTXT(newNum, "Taring");
        scale.tare();
      }
      else if (rawData == 300){
        webSocket.sendTXT(newNum, "Restarting");
        ESP.restart();
      }
    } 
      break;

    // For everything else: do nothing
    case WStype_BIN:
    case WStype_ERROR:
    case WStype_FRAGMENT_TEXT_START:
    case WStype_FRAGMENT_BIN_START:
    case WStype_FRAGMENT:
    case WStype_FRAGMENT_FIN:
    default:
      break;
  }
}

void setup() {

  Serial.begin(9600);
  
  scale.begin(dataPin, clockPin);
  scale.set_scale(127.15); //127.15 @ 20 kgs 
  scale.tare();
  
  // Connect to WiFi
  Serial.println("Connecting");
  WiFi.begin(ssid, password);
  while ( WiFi.status() != WL_CONNECTED ) {
    delay(500);
    Serial.print(".");
  }

  // Print our IP address
  Serial.println("Connected!");
  Serial.print("My IP address: ");
  Serial.println(WiFi.localIP());

  // Start WebSocket server and assign callback
  #if WS
  webSocket.begin();
  webSocket.onEvent(onWebSocketEvent);
  #endif 
}

float lbBuf[6] = {0,0,0,0,0,0};
int lbCount = 0;
long foundweight = 0;
long seenweight = 0;
long weightleft = 0;

void loop() {
    weight = scale.get_units(10);
    weight = (weight < 0) ? 0: weight;
    float lbs = ((float)weight/1000.0) * 2.205;
   /* String test = String(lbs);
    #if WS
    webSocket.sendTXT(newNum, test);
    #endif
*/

    if (lbs > 1.0 && seenweight == 0){
       seenweight = millis();
       Serial.print("\r...");
       Serial.print("                            ");
       #if WS
      webSocket.sendTXT(newNum, "...");
      #endif
    }
    else if ((millis() - seenweight) > 500 && lbCount < 5 && foundweight == 0 && lbs > 1.0){
      lbBuf[lbCount++] = lbs;
    }
    else if (lbCount >=4 && foundweight == 0){
      for (int i = 0; i<5 ; i++) lbBuf[5] += lbBuf[i];
      lbBuf[5] /= 5;
      lbCount = 0;
      Serial.print("\rLBS: ");
      Serial.print(lbBuf[5], 3);
      Serial.print("                  ");
      String test = String(lbBuf[5]);
      #if WS
      webSocket.sendTXT(newNum, test);
      #endif
      foundweight = millis();
    }
    else if (seenweight && (millis() - foundweight) > 5000 && lbs < 1.0){
      Serial.print("\r E");
      Serial.print("                 ");
      #if WS
      webSocket.sendTXT(newNum, "E");
      #endif
      seenweight = 0;
      foundweight = 0;
      lbCount = 0;
      lbBuf[5] = 0;
    }


    
    
  //  Serial.println(lbs , 3);
  
  if (Serial.available() > 0){
     char dataRX = Serial.read();
     if(dataRX == 'c'){
        Serial.print("\r Calibrating..............                                  ");
        scale.callibrate_scale(2000); //2000); //13517.05);
     }
     else if(dataRX == 'z'){
        Serial.print("\r Stopping readings..............                             ");
        scale.power_down();
     }
     else if(dataRX == 'a'){
        Serial.print("\r Starting readings..............                             ");
        scale.power_up();
     }
     else if(dataRX == 'r'){
        Serial.print("\r restarting..............                                     ");
        ESP.restart();
        //reboot();
     }
     else if(dataRX == 't'){
        Serial.print("\r Taring..............                                          ");
        scale.tare();
     }
     else if(dataRX == 'o'){
        Serial.print("\r offset: ");
        long os = scale.get_offset();
        Serial.print(os);
        Serial.print("                                                                 ");
        
     }
  }
  // Look for and handle WebSocket data
  #if WS
    webSocket.loop();
    #endif
  //delay(250);
}
