#include "loadCell.h"
//#include "HX711.h"
//2000
//4014
//6020
//8030



#define DEBUG         0
//get raw value 1
// get clean value with logic implemented
#define READ_RAW      1
//change to 0 for Node-red
#define SERIAL        1
//pins needed
#define dataPin       14//26
#define clockPin      12 //27
//deviation max in raw reading for calculations to occur
#define DEV           0.75
//buffer size of 10 -> [0:9]
#define BUF           9
#define scale_factor  -20.53 //25.865 // 19.91 
    
HX711 scale;

volatile long weight;
float weightCalc = 0;
int lbCount = 0;
float lbBuf[10] = {0,0,0,0,0,0,0,0,0,0};
float max_,avg,rng = 0.0;
float min_ = 999.0;

long foundweight = 0;
long seenweight  = 0;
long heartBeat   = 0;
String units = "grams";

void setup()
{ 
  Serial.begin(115200);
  scale.begin(dataPin, clockPin);
  scale.tare();
  scale.set_scale(scale_factor);
  
  //initScale(27, 26);
  //tare();
  //set_scale(scale_factor);
  weight = 0;

  Serial.write(27);
  Serial.print("[2J");
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\rAPCP Scale  --- 2/16/2021 ");
  Serial.println();
  Serial.println("UART controls");
  Serial.println();
  Serial.println("t - tare");
  Serial.println("r - restart");
  Serial.println("o - view offset of ADC");
  Serial.println("s - view scale");
  Serial.println("c - calibrate with 2kg (4.41 lbs)");
  Serial.println("v - calibrate with 4kg (8.82 lbs)");
  Serial.println("b - calibrate with 8kg (17.64 lbs)\n\n");
  Serial.println("Change units: ");
  Serial.println("p - pounds");
  Serial.println("g - grams\n\n");
  
  
  xTaskCreatePinnedToCore(txScale,"serial task",2048,NULL, 2,NULL, 1);
  heartBeat = millis();
}

void loop()
{
  
}

void txScale(void * param){
  while(1){
    #if !READ_RAW
      readScale();
    #else

    weight = scale.get_units(10); //readWeight();
    long raw = scale.read_average(1);
    weight = weight >= 0 ? weight : 0; 
    if (units == "lbs"){
      weightCalc = ((float)weight/1000.0) * 2.205;
    }
    else {
      weightCalc = weight;
    }
      #if SERIAL
      Serial.print("\r");
      #endif
      Serial.print(units);
      Serial.print(" : ");
      Serial.print(weightCalc);
      Serial.print(" -- ");
      Serial.print(raw);
      
      #if !SERIAL
      Serial.print("\n");
      #endif
      #if SERIAL
      Serial.print("                                           ");
      #endif
    #endif
    writeSerial();
    vTaskDelay(200/ portTICK_PERIOD_MS);
  } 
}

//clear system variable, for bad read, and weight removal
void clrSysVar()  {
  Serial.print("\rE                                             ");
  seenweight = 0;
  foundweight = 0;
  lbCount = 0;
  avg = 0.0;
  rng = 0.0;
  min_ = 999.0;
  max_ = 0;
}

void readScale(){
  weight = scale.get_units(5);
  weight = weight >= 0 ? weight : 0; 
  float lbs = ((float)weight/1000.0) * 2.205;
  //weight greater than a lb seen
  if ( lbs > 1.0 && seenweight == 0){
       seenweight = millis();
       #if SERIAL
        Serial.print("\r---                                ");
       #else
        Serial.println("---");
       #endif
  }
  //start filling buffer
  else if (lbCount < BUF + 1 && lbs > 1.0 && (millis() - seenweight) > 500){
      #if SERIAL
        Serial.print("\r...                                 ");
       #else
        Serial.println("...");
       #endif
      
      lbBuf[lbCount++] = lbs;
      //determine min max and range
      if (lbs < min_)  min_ = lbs;
      if (lbs > max_)  max_ = lbs;
      rng = max_ - min_;
      
      #if DEBUG
        Serial.print("\r");
        Serial.print("raw:");
        Serial.print(lbBuf[lbCount - 1]);
        Serial.print(" min:");
        Serial.print(min_);
        Serial.print("  max:");
        Serial.print(max_);
        Serial.print("  rng:");
        Serial.print(rng);
      #endif
      if (rng >= DEV){
         clrSysVar();
      }
    }
  else if (lbCount >= BUF && foundweight == 0){
     // for (int i = 1; i< BUF+1 ; i++) lbBuf[i+1] = lbBuf[i-1];
      if (rng < DEV){
        for (int i = 0; i< BUF + 1 ; i++) avg += lbBuf[i];
        foundweight = millis();
        avg /= BUF + 1;
        //LUT still need to figure out implementation 
        
        //if (avg >= 50.0) avg = avg *1.04;
        //if (avg >= 10.0 && avg <= 50) avg = avg *1.02;
        /* if( avg > 10 && avg < 20)  avg = avg* 1.0075;
          if ( avg >= 20 && avg < 50) avg = avg* 1.01;
          if ( avg >= 50 && avg < 70) avg = avg* 1.02;
          if ( avg >= 70) avg = avg* 1.0325;
        */
        #if SERIAL
          Serial.print("\r");
          Serial.print(avg, 3);
          Serial.print("                                         ");
        #else
          Serial.println(avg, 3);
        #endif 
      }
      else {
        lbCount = 0;
        avg = 0.0;
        rng = 0.0;
        min_ = 999.0;
        max_ = 0;
      }
    }
    else if (lbs < 1.0 && seenweight && (millis() - foundweight) > 3000){
      #if SERIAL
        Serial.print("\rE                                                    ");
      #else 
        Serial.println("Empty");
      #endif
      clrSysVar();
    }
    
    #if !SERIAL
    else if ((millis()-heartBeat) > 1000 && lbs < 1.0) {
      Serial.println("HB");
    }
    #endif 
}

//Write serial data 4from keyboard to esp
void writeSerial(){
  if (Serial.available() > 0){
     char dataRX = Serial.read();
     //switch case when more added 
     switch (dataRX)
     {
       case 'c':
        #if SERIAL
        Serial.print("\r Calibrating..............                                  ");
        #else 
        Serial.println("Calibrating....");
        #endif 
        scale.callibrate_scale(2000); //4445); //9.8lbs //11522); //
        clrSysVar();
        break;
     case 'v':
        #if SERIAL
        Serial.print("\r Calibrating..............                                  ");
        #else 
        Serial.println("Calibrating....");
        #endif 
        scale.callibrate_scale(4000); //4445); //9.8lbs //11522); //
        clrSysVar();
        break;
     case 'b':
        #if SERIAL
        Serial.print("\r Calibrating..............                                  ");
        #else 
        Serial.println("Calibrating....");
        #endif 
        scale.callibrate_scale(8000); //4445); //9.8lbs //11522); //
        clrSysVar();
        break;
     
    
     case 'r':
        #if SERIAL
        Serial.print("\r restarting..............                       ");
        #else
        Serial.println("restarting....");
        #endif
        ESP.restart();
        break;
     case 't':
        #if SERIAL 
        Serial.print("\r Taring..............                                  ");
        #else
        Serial.println("Taring....");
        #endif
        scale.tare();
        break;  
     case 'o':
     {
        Serial.print("\r offset: ");
        long os = scale.get_offset();
        Serial.print(os);
        Serial.print("                                                   ");
        break;
     }
    case 's':{
        Serial.print("\r scale: ");
        float sc = scale.get_scale();
        Serial.print(sc);
        Serial.print("                                                                 ");
        break;
    }
     case 'g':{
        Serial.print("\r changing units to ");
        units = "grams";
        Serial.print(units);
        Serial.print("                                    ");
        break;
     }
     case 'k':{
        Serial.print("\r changing units to ");
        units = "kilograms";
        Serial.print(units);
        Serial.print("                                    ");
        break;
     } 
     case 'p':{
        Serial.print("\r changing units to ");
        units = "lbs";
        Serial.print(units);
        Serial.print("                                    ");
        break;
     }
     }
     }
  }   









/*
if(dataRX == 'c'){
        #if SERIAL
        Serial.print("\r Calibrating..............                                  ");
        #else 
        Serial.println("Calibrating....");
        #endif 
        scale.callibrate_scale(2000); //4445); //9.8lbs //11522); //
        clrSysVar();
     }
     else if(dataRX == 'r'){
        #if SERIAL
        Serial.print("\r restarting..............                       ");
        #else
        Serial.println("restarting....");
        #endif
        ESP.restart();
     }
     else if(dataRX == 't'){
        #if SERIAL 
        Serial.print("\r Taring..............                                  ");
        #else
        Serial.println("Taring....");
        #endif
        scale.tare();  
     }
     else if(dataRX == 'o'){
        Serial.print("\r offset: ");
        long os = scale.get_offset();
        Serial.print(os);
        Serial.print("                                                   ");
        
     }
     else if(dataRX == 's'){
        Serial.print("\r scale: ");
        float s = scale.get_scale();
        Serial.print(s);
        Serial.print("                                                                 ");
        
     }
   }


*/









/* if (EEPROM.begin(E_SIZE)) Serial.println("EEPROM initalized");
  Serial.println(EEPROM.read(0));
  if (!EEPROM.read(0)) {
    Serial.println("Add calibration weight then press C");
    if (Serial.available() > 0){
     char dataRX; 
     while (dataRX != 'c') dataRX = Serial.read();
    }
    long os = scale.get_offset();
    Serial.print(os);
    float s = scale.get_scale();
    Serial.print(s);

    EEPROM.write(0,os>>);
    EEPROM.commit();

  }
  delay(10000);
  */
