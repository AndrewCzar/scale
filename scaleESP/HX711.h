#ifndef __HX711__
#define  __HX711__

#include "Arduino.h"

extern uint8_t _dataPin;
extern uint8_t _clockPin;
extern uint32_t _offset;
extern float    _scale;



void initScale(uint8_t dataPin, uint8_t clockPin);
void set_scale(float scale = 1.0);
void tare(uint8_t times = 10) ;
long readRaw();
uint8_t shiftIn(uint8_t dataPin, uint8_t clockPin);
float read_average(uint8_t times);
float readWeight();


#endif