#include "HX711.h"

uint8_t  _dataPin;
uint8_t  _clockPin;
uint32_t _offset;
float    _scale;


void initScale(uint8_t dataPin, uint8_t clockPin)
{
  _dataPin = dataPin;
  _clockPin = clockPin;

  pinMode(_dataPin, INPUT);
  pinMode(_clockPin, OUTPUT);
  digitalWrite(_clockPin, 0);
}

void set_scale(float scale)  { 
    _scale = 1.0 / scale; 
}

void tare(uint8_t times)      { 
    _offset = read_average(times); 
}

long readRaw() 
{
  // might have to add delay 
  while (digitalRead(_dataPin) == 1);//delayMicroseconds(1);
  
  union
  {
    long value = 0;
    uint8_t data[4];
  } v;

  noInterrupts();

  // Pulse the clock pin 24 times to read the data.
  v.data[2] = shiftIn(_dataPin, _clockPin);
  v.data[1] = shiftIn(_dataPin, _clockPin);
  v.data[0] = shiftIn(_dataPin, _clockPin);

  digitalWrite(_clockPin, 1);
  digitalWrite(_clockPin, 0);

  interrupts();

  // SIGN extend
  if (v.data[2] & 0x80) v.data[3] = 0xFF;
  return 1.0 * v.value;
}


uint8_t shiftIn(uint8_t dataPin, uint8_t clockPin) 
{
    uint8_t value = 0;
    uint8_t i;

    for(i = 0; i < 8; ++i) {
        digitalWrite(clockPin, 1);
        delayMicroseconds(1);
        value |= digitalRead(dataPin) << (7 - i);
        digitalWrite(clockPin, 0);
        delayMicroseconds(1);
    }
    return value;
}


float read_average(uint8_t times) 
{
  float sum = 0;
  for (uint8_t i = 0; i < times; i++) 
  {
    sum += readRaw();
  }
  return sum / times;
}


float readWeight(){
    float lbs = (read_average(5) - _offset) * _scale;
    return lbs;
}