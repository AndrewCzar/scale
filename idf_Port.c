
#include "idf_Port.h"


void init_HX711(HX711 *self, uint8_t data, uint8_t clock){
    self->_dta = (gpio_num_t)data;
    self->_clk = (gpio_num_t)clock;
    gpio_set_direction(self->_dta, GPIO_MODE_INPUT);
    gpio_set_direction(self->_clk, GPIO_MODE_OUTPUT);
    gpio_set_level(self->_clk, 0);
    reset_HX711(self);

}

void reset_HX711(HX711 *self){
    self->_offset = 0;
    self->_gain   = 0;
    self->_scale  = 0;
}


uint8_t shiftByte(HX711 *self, uint8_t bitOrder){
    uint8_t data    = 0;
    uint8_t cnt     = 0;

    for(cnt = 0; cnt< 8; ++cnt){
        gpio_set_level(self->_clk, 1);
        ets_delay_us(1);
        data |= (bitOrder) ? (gpio_get_level(self->_clk) << 7 - cnt) : (gpio_get_level(self->_clk) << cnt);
        gpio_set_level(self->_clk, 0);
        ets_delay_us(1);
    }
    return data;
}


