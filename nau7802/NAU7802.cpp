#include "NAU7802.h"

_PU_CTRL_REG pu_ctrl_reg;

_CTRL1_REG ctrl1_reg;

_CTRL2_REG ctrl2_reg;

_PGA_REG pga_reg;

_PWR_REG pwr_reg;

  pu_ctrl_reg.REG_RST = 0;
  pu_ctrl_reg.POW_DIG = 1;
  pu_ctrl_reg.POW_ALG = 0;
  pu_ctrl_reg.POW_DIG = 1;

uint8_t init_NAU7802(){
  //Wire.begin(SDA,SCL);

  
  Wire.beginTransmission(defaultAddress);
  if(Wire.endTransmission() != 0)   return 0; 
  return 1;
}

uint8_t rdReg(uint8_t reg){
  Wire.beginTransmission(defaultAddress);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(defaultAddress, 1);
  uint8_t  data  = Wire.read(); 
  return data;
}


uint8_t wrReg(uint8_t reg, uint8_t data){
  Wire.beginTransmission(defaultAddress);
  Wire.write(reg);
  Wire.write(data);
  uint8_t ack = Wire.endTransmission(); 
  return ack;
}
