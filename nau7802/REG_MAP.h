#ifndef _REG_MAP
#define _REG_MAP

#define PU_CTRL         0x00
#define CTRL1           0x01
#define CTRL2           0x02
#define OCAL1_B2        0x03
#define OCAL1_B1        0x04
#define OCAL1_B0        0x05
#define GCAL1_B3        0x06
#define GCAL1_B2        0x07
#define GCAL1_B1        0x08
#define GCAL1_B0        0x09
#define OCAL2_B2        0x0A
#define OCAL2_B1        0x0B
#define OCAL2_B0        0x0C
#define GCAL2_B3        0x0D
#define GCAL2_B2        0x0E
#define GCAL2_B1        0x0F
#define GCAL2_B0        0x10
#define I2C_CTRL        0x11
#define ADC0_B2         0x12
#define ADC0_B1         0x13
#define ADC0_B0         0x14
#define OTP_B1          0x15
#define OTP_B0          0x16
#define DEV_REV_CODE    0X1F

//default 0x00
typedef struct {
  uint8_t  REG_RST  :1;
  uint8_t  POW_DIG  :1;
  uint8_t  POW_ALG  :1;
  uint8_t  POW_RDY  :1;
  uint8_t  CYC_STR  :1;
  uint8_t  CYC_RDY  :1;
  uint8_t  SYS_CLK  :1;
  uint8_t  AVDDSS   :1; 
  
}_PU_CTRL_REG;      

//default 0x00
typedef struct{
  uint8_t  GN       :3;
  uint8_t  VLDO     :3;
  uint8_t  DRNY_SEL :1;
  uint8_t  CRP      :1; 
}_CTRL1_REG;


typedef struct{
  uint8_t  CMD      :2;
  uint8_t  CALS     :1;
  uint8_t  CAL_ERR  :1;
  uint8_t  CRS      :3;
  uint8_t  CHS      :1; 
}_CTRL2_REG;

typedef struct {
    uint8_t  CHP_DIS          :1;
    uint8_t  RES              :2;
    uint8_t  PGAINV           :1;
    uint8_t  BPASS_EN         :1;
    uint8_t  OBUF_EN          :1;
    uint8_t  LDOMODE          :1;
    uint8_t  RD_OTP_SEL       :1;
}_PGA_REG;


typedef struct {
  uint8_t  PGA_CUR          :2;
  uint8_t  ADC_CUR          :2; 
  uint8_t  MBIAS_CUR        :3;
  uint8_t  PGA_CAP_EN       :1;
}_PWR_REG;

/*
typedef struct {
  uint8_t  BGPCP  :1;
  uint8_t  TS     :1;
  uint8_t  BOPGA  :1;
  uint8_t  SI     :1;
  uint8_t  WPD    :1;
  uint8_t  SPE    :1;
  uint8_t  FRD    :1;
  uint8_t  CRSD   :1; 
  
}I2C_CTRL_REG;  
*/



#endif 
