/*
  This example merely outputs the raw data from a load cell. For example, the
  output may be 25776 and change to 43122 when a cup of tea is set on the scale.
  These values are unitless - they are not grams or ounces. Instead, it is a
  linear relationship that must be calculated. Remeber y = mx + b?
  If 25776 is the 'zero' or tare state, and 43122 when I put 15.2oz of tea on the
  scale, then what is a reading of 57683 in oz?

  (43122 - 25776) = 17346/15.2 = 1141.2 per oz
  (57683 - 25776) = 31907/1141.2 = 27.96oz is on the scale

*/


#define DEBUG         0
//get raw value 1
// get clean value with logic implemented
#define READ_RAW      1
//change to 0 for Node-red
#define SERIAL        1
//pins needed
#define dataPin       26
#define clockPin      27
//deviation max in raw reading for calculations to occur
#define DEV           0.75
//buffer size of 10 -> [0:9]
#define BUF           9
#define scale_factor  -20.53 //25.865 // 19.91 


#include <Wire.h>
//#include <EEPROM.h>
#include "SparkFun_Qwiic_Scale_NAU7802_Arduino_Library.h" // Click here to get the library: http://librarymanager/All#SparkFun_NAU7802

NAU7802 myScale; //Create instance of the NAU7802 class
#define LOCATION_CALIBRATION_FACTOR 0 //Float, requires 4 bytes of EEPROM
#define LOCATION_ZERO_OFFSET 10 //Must be more than 4 away from previous spot. Long, requires 4 bytes of EEPROM

bool settingsDetected = false; //Used to prompt user to calibrate their scale

//Create an array to take average of weights. This helps smooth out jitter.
#define AVG_SIZE 50
float avgWeights[AVG_SIZE];
byte avgWeightSpot = 0;

long offset;
int first = 0;
long timer = 0;

void setup()
{
  Serial.begin(115200);
  Serial.write(27);
  Serial.print("[2J");
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\rAPCP Scale  --- 2/16/2021 ");
  Serial.println();
  Serial.println("UART controls");
  Serial.println();
  Serial.println("t - tare");
  Serial.println("r - restart");
  Serial.println("o - view offset of ADC");
  Serial.println("s - view scale");
  Serial.println("c - calibrate with 2kg (4.41 lbs)");
  Serial.println("v - calibrate with 4kg (8.82 lbs)");
  Serial.println("b - calibrate with 8kg (17.64 lbs)\n\n");
  Serial.println("Change units: ");
  Serial.println("p - pounds");
  Serial.println("g - grams\n\n");
  
  Wire.begin(22,23);
  Wire.setClock(400000);
   /* while(1){
    scanI2C();
  }*/
  //scanI2C();
  scanI2C();
  delay(100);
  //while(1){
    
    delay(200);
    
  //}
  
  if (myScale.begin() == false)
  {
  //  Serial.println("Scale not detected. Please check wiring. Freezing...");
    while (1);
  }

  //myscale.setRegister(

  
 // Serial.print("Zero offset: ");
 // Serial.println(myScale.getZeroOffset());
//  Serial.print("Calibration factor: ");
 /// Serial.println(myScale.getCalibrationFactor());

  //readSystemSettings(); //Load zeroOffset and calibrationFactor from EEPROM
  //delay(1000);
 // Serial.print("Mem offset: ");
 // Serial.println(myScale.getZeroOffset());
 // Serial.print("Mem Calibration factor: ");
 // Serial.println(myScale.getCalibrationFactor());
 // delay(1000);

    Wire.beginTransmission(0x2A);
    Wire.write(0x00);
    Wire.write(0x8E);
    Wire.endTransmission();
    Serial.println("PU control register set");

    Wire.beginTransmission(0x2A);
    Wire.write(0x00);
    Wire.endTransmission();
    Wire.requestFrom(0x2A,1);
    uint8_t dataRx = Wire.read();
    Serial.print("bits-> 0x");
    Serial.println(dataRx,HEX);
    delay(1000);
    myScale.setSampleRate(NAU7802_SPS_320); //Increase to max sample rate
    myScale.calibrateAFE(); 
    myScale.calculateZeroOffset(8); 
  
  delay(1000);
  timer = millis();
}

long avgTimer = 0;
float avgWeight = 0;
void loop()
{
  writeSerial();
  
  //if(myScale.available() == true)// && ((millis() - timer) > 650))
  //{ 
    long currentReading = myScale.getReading();
    float currentWeight = myScale.getWeight();
    
    Serial.print("\rReading: ");
    Serial.print(currentReading);
    Serial.print("  Weight: ");
    Serial.print(currentWeight, 2); //Print 2 decimal places
    Serial.print("   ");
    avgWeights[avgWeightSpot++] = currentWeight;
    if(avgWeightSpot == AVG_SIZE) avgWeightSpot = 0;

    
    for (int x = 0 ; x < AVG_SIZE ; x++)
      avgWeight += avgWeights[x];
    avgWeight /= AVG_SIZE;
    
    Serial.print("  AvgWeight: ");
    Serial.print(avgWeight, 2); //Print 2 decimal places*/
    Serial.print("                          ");
   /* if(settingsDetected == false)
    {
    //  Serial.print("\tScale not calibrated. Press 'c'.");
    }*/
    avgWeight = 0;
   // printSerial(" ", (String)currentReading, (String)currentWeight);
    
    delay(200);
   // timer = millis();
 // }
}



void printSerial(String msg, String raw, String weight){
  String txMsg = msg + " , " + raw + " , " + weight;
  Serial.println(txMsg);
}


void writeSerial(){
  if (Serial.available() > 0){
     char dataRX = Serial.read();
     //switch case when more added 
     switch (dataRX)
     {
       case 'c':
        #if SERIAL
        Serial.print("\r Calibrating..............                                  ");
        #else 
        Serial.println("Calibrating....");
        #endif 
        calibrateScale();
        break;
    /*  case 'v':
        #if SERIAL
        Serial.print("\r Calibrating 2kg..............                                  ");
        #else 
        Serial.println("Calibrating....");
        #endif 
        calibrateKg(2.00);*/
      case 'r':
        #if SERIAL
        Serial.print("\r restarting..............                       ");
        #else
        Serial.println("restarting....");
        #endif
        ESP.restart();
        break;
    case 't':
        #if SERIAL 
        Serial.print("\r Taring..............                                  ");
        #else
        Serial.println("Taring....");
        myScale.calculateZeroOffset();
        #endif
        
        break;  
     case 'o':
     {
        Serial.print("\r offset: ");
        long os = myScale.getZeroOffset();
        Serial.print(os);
        Serial.print("                                                   ");
        delay(1000);
        break;
     }
    case 's':{
        Serial.print("\r scale: ");
        float s = myScale.getCalibrationFactor();
        Serial.print(s);
        Serial.print("                                                                 ");
        delay(1000);
        break;
    }
     }
  }   
}


void calibrateScale(void)
{
  Serial.println();
  Serial.println();
  Serial.println(F("Scale calibration"));

  Serial.println(F("Setup scale with no weight on it. Press a key when ready."));
  while (Serial.available()) Serial.read(); //Clear anything in RX buffer
  while (Serial.available() == 0) delay(10); //Wait for user to press key

  myScale.calculateZeroOffset(64); //Zero or Tare the scale. Average over 64 readings.
  Serial.print(F("New zero offset: "));
  Serial.println(myScale.getZeroOffset());

  Serial.println(F("Place known weight on scale. Press a key when weight is in place and stable."));
  while (Serial.available()) Serial.read(); //Clear anything in RX buffer
  while (Serial.available() == 0) delay(10); //Wait for user to press key

  Serial.print(F("Please enter the weight, without units, currently sitting on the scale (for example '4.25'): "));
  while (Serial.available()) Serial.read(); //Clear anything in RX buffer
  while (Serial.available() == 0) delay(10); //Wait for user to press key

  //Read user input
  float weightOnScale = Serial.parseFloat();
  Serial.println();

  myScale.calculateCalibrationFactor(weightOnScale, 64); //Tell the library how much weight is currently on it
  Serial.print(F("New cal factor: "));
  Serial.println(myScale.getCalibrationFactor(), 2);

  Serial.print(F("New Scale Reading: "));
  Serial.println(myScale.getWeight(), 2);

//  recordSystemSettings(); //Commit these values to EEPROM
}

void calibrateKg(float weight){
  /*Serial.println();
  Serial.println();
  Serial.println(F("Scale calibration"));

  Serial.println(F("Setup scale with no weight on it. Press a key when ready."));
  while (Serial.available()) Serial.read(); //Clear anything in RX buffer
  while (Serial.available() == 0) delay(10); //Wait for user to press key
  
  myScale.calculateZeroOffset(64); //Zero or Tare the scale. Average over 64 readings.
  Serial.print(F("New zero offset: "));
  Serial.println(myScale.getZeroOffset());

  Serial.println(F("Place known weight on scale. Press a key when weight is in place and stable."));
  while (Serial.available()) Serial.read(); //Clear anything in RX buffer
  while (Serial.available() == 0) delay(10); //Wait for user to press key
  */
  /*Serial.print(F("Please enter the weight, without units, currently sitting on the scale (for example '4.25'): "));
  while (Serial.available()) Serial.read(); //Clear anything in RX buffer
  while (Serial.available() == 0) delay(10); //Wait for user to press key
  */
  //Read user input
  //float weightOnScale = Serial.parseFloat();
  //Serial.println();

  myScale.calculateCalibrationFactor(weight, 64); //Tell the library how much weight is currently on it
  Serial.print(F("New cal factor: "));
  Serial.println(myScale.getCalibrationFactor(), 2);

  Serial.print(F("New Scale Reading: "));
  Serial.println(myScale.getWeight(), 2);
}

void scanI2C(){
  byte error, address;
  int nDevices;
 
  Serial.println("Scanning...");
 
  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");
 
  delay(4000);
}


//Record the current system settings to EEPROM
/*void recordSystemSettings(void)
{
  //Get various values from the library and commit them to NVM
  EEPROM.put(LOCATION_CALIBRATION_FACTOR, myScale.getCalibrationFactor());
  EEPROM.put(LOCATION_ZERO_OFFSET, myScale.getZeroOffset());
}

//Reads the current system settings from EEPROM
//If anything looks weird, reset setting to default value
void readSystemSettings(void)
{
  float settingCalibrationFactor; //Value used to convert the load cell reading to lbs or kg
  long settingZeroOffset; //Zero value that is found when scale is tared

  //Look up the calibration factor
  EEPROM.get(LOCATION_CALIBRATION_FACTOR, settingCalibrationFactor);
  if (settingCalibrationFactor == 0xFFFFFFFF)
  {
    settingCalibrationFactor = 0; //Default to 0
    EEPROM.put(LOCATION_CALIBRATION_FACTOR, settingCalibrationFactor);
  }

  //Look up the zero tare point
  EEPROM.get(LOCATION_ZERO_OFFSET, settingZeroOffset);
  if (settingZeroOffset == 0xFFFFFFFF)
  {
    settingZeroOffset = 1000L; //Default to 1000 so we don't get inf
    EEPROM.put(LOCATION_ZERO_OFFSET, settingZeroOffset);
  }

  //Pass these values to the library
  myScale.setCalibrationFactor(settingCalibrationFactor);
  myScale.setZeroOffset(settingZeroOffset);

  settingsDetected = true; //Assume for the moment that there are good cal values
  if (settingCalibrationFactor < 0.1 || settingZeroOffset == 1000)
    settingsDetected = false; //Defaults detected. Prompt user to cal scale.
}
*/
