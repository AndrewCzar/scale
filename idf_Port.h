#include "rom/ets_sys.h"

#include "driver/gpio.h"


#define LSB 0
#define MSB 1


typedef struct{
    uint8_t     _dta;
    uint8_t     _clk;
    uint8_t     _gain;
    uint8_t     _scale;
    uint32_t    _offset; 
}HX711;


