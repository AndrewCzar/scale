#include "scale.h"

void initScale(dataRegInit *dataRx){
  //reset all registers by toggling reg 0x00 bit 1
  writeReg8(0x2A,0x00,0x01);
  writeReg8(0x2A,0x00,0x00);
  
  //power up analog and digital
  writeReg8(0x2A,0x00,0x8E);
  dataRx->reg0 = readReg8(0x2A,0x00); 
  
  //set ldo voltage 3.0 V
  writeReg8(0x2A,0x01,0x3F);
  dataRx->reg1 = readReg8(0x2A,0x01); 
  
  //set sample rate 80 SPS
  writeReg8(0x2A,0x02,0x20);
  dataRx->reg2 = readReg8(0x2A,0x02);
  
  // turn off CLK_CHP
  writeReg8(0x2A,0x15,0x30);
  dataRx->reg15 = readReg8(0x2A,0x15); 
}

uint8_t setCalBit(){
  //set bit 7 in register 2, this retares the chip.
  setBit(2,0x02);
  //uint8_t dataRx = getBit(2,0x02);
  //if (dataRx){
  while(readReg8(0x2A,0x02) & 0b00000100) delay(100);    //Serial.print("\r....");
  uint8_t dataRx = readReg8(0x2A,0x02);
  return dataRx;
  //}
  //else return 0xEE;
}


uint8_t writeReg8(uint8_t addr,uint8_t reg, uint8_t tx){
  Wire.beginTransmission(addr);
  Wire.write(reg);
  Wire.write(tx);
  uint8_t st =  Wire.endTransmission();
  return st;
}

uint8_t readReg8(uint8_t addr,uint8_t reg){
  Wire.beginTransmission(addr);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(addr,1);
  uint8_t dataRx = Wire.read();
  return dataRx;
}

uint32_t readReg24(uint8_t addr,uint8_t reg){
  Wire.beginTransmission(addr);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(addr,3);
  uint32_t dataRx = Wire.read() << 16;
  dataRx |= Wire.read()<< 8;
  dataRx |= Wire.read();
  return dataRx;
}

bool setBit(uint8_t bitNumber, uint8_t registerAddress)
{
  uint8_t value = readReg8(0x2A, registerAddress);
  value |= (1 << bitNumber); //Set this bit
  return (writeReg8(0x2A,registerAddress, value));
}

//Mask & clear a given bit within a register
bool clearBit(uint8_t bitNumber, uint8_t registerAddress)
{
  uint8_t value = readReg8(0x2A,registerAddress);
  value &= ~(1 << bitNumber); //Set this bit
  uint8_t st =  writeReg8(0x2A,registerAddress, value);
  return st;
}

//Return a given bit within a register
bool getBit(uint8_t bitNumber, uint8_t registerAddress)
{
  uint8_t value = readReg8(0x2A,registerAddress);
  value &= (1 << bitNumber); //Clear all but this bit
  return (value);
}
