
//#include "SparkFun_Qwiic_Scale_NAU7802_Arduino_Library.h"
#include <Wire.h>

#include "scale.h"
 //#include "scale.cpp"
//registers

//0x00 


void setup() {
  Serial.begin(115200);
  Wire.begin(22,23);
  Wire.setClock(400000);
  scanI2C();
  
 /* Wire.beginTransmission(0x2A);
  Wire.write(0x00);
  Wire.write(0x8E);
  Wire.endTransmission();
*/
  //reset register
  writeReg8(0x2A,0x00,0x01);
  Serial.println("reseting");
  delay(100);
  writeReg8(0x2A,0x00,0x00);
  Serial.println("Done\n");
  
  //power up analog and digital
  writeReg8(0x2A,0x00,0x8E);
  Serial.println("PU control register set\n");
  delay(100);
  uint8_t dataRx = readReg8(0x2A,0x00); //Wire.read();
  Serial.print("0x00 bits-> 0x");
  Serial.println(dataRx,HEX);
  //set ldo voltage
  Serial.println("setting LDO");
  writeReg8(0x2A,0x01,0x3F);//0xA3);
  delay(100);
  dataRx = readReg8(0x2A,0x01); //Wire.read();
  Serial.print("0x01 bits-> 0x");
  Serial.println(dataRx,HEX);
  //set sample rate
  Serial.println("set sample rate");
  writeReg8(0x2A,0x02,0x20);//0xA3);
  delay(100);
  dataRx = readReg8(0x2A,0x02); //Wire.read();
  Serial.print("0x02 bits-> 0x");
  Serial.println(dataRx,HEX);

  Serial.println("turn off CLK_CHP");
  writeReg8(0x2A,0x15,0x30);//0xA3);
  delay(100);
  dataRx = readReg8(0x2A,0x15); //Wire.read();
  Serial.print("0x15 bits-> 0x");
  Serial.println(dataRx,HEX);

  Serial.println("setting calibration bit\n");
  setBit(2,0x02);
  dataRx = readReg8(0x2A,0x02); //Wire.read();
  Serial.print("0x02 bits-> 0x");
  Serial.println(dataRx,HEX);

  while(readReg8(0x2A,0x02) & 0b00000100) Serial.print("\r....");
  Serial.print("\n");
  Serial.println("adc initialized\n");

  dataRx = readReg8(0x2A,0x02); //Wire.read();
  Serial.print("0x02 bits-> 0x");
  Serial.println(dataRx,HEX);
   

}

uint32_t dataRx = 0;

void loop() {
  // put your main code here, to run repeatedly:
  
 /* Wire.beginTransmission(0x2A);
  Wire.write(0x12);
  Wire.endTransmission();
  Wire.requestFrom(0x2A,3);
  dataRx |= Wire.read() << 16;
  dataRx |= Wire.read()<< 8;
  dataRx |= Wire.read();
  Serial.print("\r");
  Serial.print(dataRx);
  Serial.print("                       ");
  */
  
  while (!getBit(5,0x00)) Serial.print("\rwaiting for data..         ");
  uint32_t scaleRx = readReg24(0x2A,0x12);
  Serial.print("\rRaw : ");
  Serial.print(scaleRx);
  Serial.print("                ");
  delay(250);
}






void scanI2C(){
  byte error, address;
  int nDevices;
 
  Serial.println("Scanning...");
 
  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");
 
  delay(2000);
}
