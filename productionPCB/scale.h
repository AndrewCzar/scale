#ifndef scale__h
#define scale__h
//#include "stdint.h"

#include "dataTypes.h"
//#include "Arduino.h"
#include "Wire.h"

#define PU_CTRL         0x00
#define CTRL1           0x01
#define CTRL2           0x02
#define OCAL1_B2        0x03
#define OCAL1_B1        0x04
#define OCAL1_B0        0x05
#define GCAL1_B3        0x06
#define GCAL1_B2        0x07
#define GCAL1_B1        0x08
#define GCAL1_B0        0x09
#define OCAL2_B2        0x0A
#define OCAL2_B1        0x0B
#define OCAL2_B0        0x0C
#define GCAL2_B3        0x0D
#define GCAL2_B2        0x0E
#define GCAL2_B1        0x0F
#define GCAL2_B0        0x10
#define I2C_CTRL        0x11
#define ADC0_B2         0x12
#define ADC0_B1         0x13
#define ADC0_B0         0x14
#define OTP_B1          0x15
#define OTP_B0          0x16
#define DEV_REV_CODE    0X1F

#define ADDR 0x2A

typedef struct {
  uint8_t reg0;
  uint8_t reg1;
  uint8_t reg2;
  uint8_t reg15;
}dataRegInit;

void initScale(dataRegInit *dataRx);

uint8_t setCalBit();

uint8_t writeReg8(uint8_t addr,uint8_t reg, uint8_t tx);

uint8_t readReg8(uint8_t addr,uint8_t reg);

uint32_t readReg24(uint8_t addr,uint8_t reg);

bool setBit(uint8_t bitNumber, uint8_t registerAddress);

//Mask & clear a given bit within a register
bool clearBit(uint8_t bitNumber, uint8_t registerAddress);

//Return a given bit within a register
bool getBit(uint8_t bitNumber, uint8_t registerAddress);



#endif
