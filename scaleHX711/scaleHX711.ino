#include "loadCell.h"
//#include <avr/wdt.h>
#include "EEPROM.h"

HX711 scale;

uint8_t dataPin = 27;//3;
uint8_t clockPin = 26;//4;

uint32_t start, stop;
volatile long f[3];

void setup()
{ 
  Serial.begin(9600);
  Serial.write(27);
  Serial.print("[2J");
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\rAPCP Scale  --- 12/17/2020 ");
  Serial.println();
  Serial.println("UART controls");
  Serial.println();
  Serial.println("t - tare");
  Serial.println("r - restart");
  Serial.println("c - calibrate with 2kg");
  Serial.println("o - view offset of ADC");
  Serial.println("g - view PGA gain ");
  Serial.println();
  Serial.println();
   scale.begin(dataPin, clockPin);
   // loadcell factor 20 KG
   scale.set_scale(127.15); //127.15 @ 20 kgs 
   scale.tare();
   //scale.callibrate_scale(1, 5);
   f[1] = 0;
}

float lbBuf[3] = {0,0,0};
int lbCount = 0;

void loop()
{
     
    f[0] = scale.get_units(10);
    
    //f[0] = scale.read_average(20);
    //f[0] = ~(f[0]-1); //twos compliment to normal binary
    //long lbs = f[0] + scale.get_offset();
    
    f[0] = f[0] >= 0 ? f[0]: 0; 
    //Serial.println(f[0]);
    float lbs = ((float)f[0]/1000.0) * 2.205;
    
    if (lbs != 0){          //lbCount++] 
       if (lbs > lbBuf[1]) lbBuf[1] = lbs;
       if (lbCount = 2) lbCount = 0;
    }
  //  Serial.print("\rGrams: ");
  //  Serial.print(f[0]);
    Serial.print("\rKgs: ");
    Serial.print((float)f[0]/1000.0 , 3);
    Serial.print("  lbs: ");
    Serial.print(lbs , 3);
    Serial.print("  Largest Object: ");
    Serial.print(lbBuf[1] , 3);
    Serial.print("              ");

   
    
    //Serial.println(lbs , 3);
    
    
  if (Serial.available() > 0){
     char dataRX = Serial.read();
     if(dataRX == 'c'){
        Serial.print("\r Calibrating..............                                  ");
        scale.callibrate_scale(2000); //2000); //13517.05);
     }
     else if(dataRX == 'z'){
        Serial.print("\r Stopping readings..............                             ");
        scale.power_down();
     }
     else if(dataRX == 'a'){
        Serial.print("\r Starting readings..............                             ");
        scale.power_up();
     }
     else if(dataRX == 'r'){
        Serial.print("\r restarting..............                                     ");
        ESP.restart();
        //reboot();
     }
     else if(dataRX == 't'){
        Serial.print("\r Taring..............                                          ");
        scale.tare();
     }
     else if(dataRX == 'o'){
        Serial.print("\r offset: ");
        long os = scale.get_offset();
        Serial.print(os);
        Serial.print("                                                                 ");
        
     }
     else if(dataRX == 'g'){
        Serial.print("\r gain: ");
        long gn = scale.get_gain();
        Serial.print(gn); 
        Serial.print("                                                                 ");
        
     }
  }
  
  delay(50);
}
/*
void reboot() {
  wdt_disable();
  wdt_enable(WDTO_15MS);
  while (1) {}
}*/
